package timelinejavaavancedenisnoel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;


import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class Jeu extends JFrame implements Serializable{
	
	/**
	 * attribut liste des joueurs
	 */
	private ArrayList<Joueur> joueurs;
	/**
	 * attribut boite de cartes jou�es fausses
	 */
	private ArrayList<Carte> boite;
	/**
	 * attribut paquet de cartes non jou�es
	 */
	private ArrayList<Carte> paquet;
	/**
	 * attribut cartes sur le terrain
	 */
	private ArrayList<Carte> terrain;
	/**
	 * attribut carte selectionnee par le joueur
	 */
	private int carteSelectionnee;
	/**
	 * attribut indice du joueur
	 */
	private int indiceJoueur;
	/**
	 * attribut classement global du jeu ( scores )
	 */
	private Classement cl;
	
	/**
	 * attribut de construction des diff�rents paquets
	 */
	private ConstruirePaquet lf;
	
	/**
	 * Constucteur vide de la classe jeu
	 */
	public Jeu(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.joueurs = new ArrayList<Joueur>();
		this.boite = new ArrayList<Carte>();
		this.paquet = new ArrayList<Carte>();
		this.terrain = new ArrayList<Carte>();
		this.cl = new Classement();
		this.lf = new ConstruirePaquet(this);
	}

	/**
	 * methode permettant de demarrer la partie avec le jeu selectionne 
	 * @param int nbJoueur nombre de joueurs
	 * @param JPanel jp interface
	 * @param int iterateurJoueur iterateur de joueurs
	 * @param String jeu jeu choisi
	 */
	public void demarrer(int nbJoueur, JPanel jp, int iterateurJoueur,String jeu){
		this.revalidate();
		this.repaint();

		if ( jeu.equals("cardline") ){
			lf.construireCardline();
		} else {
			lf.construireTimeline();
		}

		int nbCartes = 0;
		if ( nbJoueur == 2 || nbJoueur == 3 ){
			nbCartes = 6;
		} else if ( nbJoueur == 4 || nbJoueur == 5 ){
			nbCartes = 5;
		} else if ( nbJoueur >= 6 && nbJoueur <= 8 ){
			nbCartes = 4;
		}
		
		if( iterateurJoueur <= nbJoueur ){
			this.construireJoueur(nbCartes, iterateurJoueur, jp, nbJoueur,jeu);
		}else{
			Random r = new Random();
			this.indiceJoueur = this.obtenirPremierJoueur();
			int nbAleatoire = r.nextInt(this.paquet.size());
			this.terrain.add(this.paquet.get(nbAleatoire));
			this.paquet.remove(this.paquet.get(nbAleatoire));
			jp.removeAll();
		    jp.validate();
		    this.setLayout(new BorderLayout());
			jouer();
		}
	}
	
	/**
	 * methode permettant de jouer au jeu avec les differentes interractions
	 */
	public void jouer(){
		this.setResizable(true);
		this.revalidate();
		this.repaint();
		
		JPanel north = new JPanel();
		
		JPanel south = new JPanel();

		HashMap<JButton, Integer> map = new HashMap<JButton, Integer>();
		for (int i = 0; i < this.joueurs.get(indiceJoueur).getCartes().size(); i++) {
			JButton jb = new JButton();
			map.put(jb, i);
			jb.setIcon(new ImageIcon(this.joueurs.get(indiceJoueur).getCartes().get(i).getCarteNom() + ".jpeg"));

			jb.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setCarteSelectionnee(map.get(e.getSource()));
				}
			});
			south.add(jb);
		}
		
		JLabel pseudo = new JLabel(this.joueurs.get(indiceJoueur).getPseudonyme());
		south.add(pseudo);
		
		this.add(south, BorderLayout.SOUTH);
		
		if(south.getComponentCount() == 1){
			this.setEnabled(false);
            JFrame victoire = new JFrame();
            JPanel jp1 = new JPanel();
            JLabel jl1 = new JLabel("Le joueur " + indiceJoueur + " " + this.joueurs.get(indiceJoueur).getPseudonyme() + " a gagn�");
            jp1.add(jl1);
            cl.reconstruitClassement(this.joueurs.get(indiceJoueur));
            ArrayList<Joueur> classement = cl.getClassement();
            JPanel jp2 = new JPanel();
            for(int i = classement.size()-1; i >= 0; i--){
                JLabel jl2 = new JLabel(classement.get(i).getPseudonyme() + " : " + classement.get(i).getPointsClassement());
                jp2.add(jl2);
            }

            JPanel jp3 = new JPanel();
            JButton ok = new JButton(String.valueOf("OK"));

            ok.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setEnabled(true);
                    System.exit(0);
                }
            });
            jp3.add(ok);
            victoire.add(jp1, BorderLayout.NORTH);
            victoire.add(jp2, BorderLayout.CENTER);
            victoire.add(jp3, BorderLayout.SOUTH);
            victoire.pack();
            victoire.setVisible(true);
		}

		JPanel center = new JPanel();
		JScrollPane pane = new JScrollPane(center);
		pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	    pane.setBounds(0, 0, 1000, 1000);
		for (int i = 0; i <= this.terrain.size(); i++) {
			JLabel jl = new JLabel();
			if( i != this.terrain.size()){
				jl.setIcon(new ImageIcon(this.terrain.get(i).getCarteNomReponse() + ".jpeg"));
			}
			JButton jb = new JButton(String.valueOf(i));

			jb.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int placement = Integer.parseInt(e.getActionCommand());
					if ( placement == 0 ) {
						boolean res = joueurs.get(indiceJoueur).getCartes().get(carteSelectionnee).trier(terrain.get(0), 0);
						if ( res ) {
							terrain.add(placement, joueurs.get(indiceJoueur).getCartes().get(carteSelectionnee));
						} else {
							nouvelleCarteJoueur(indiceJoueur, getCarteSelectionnee());
						}
					} else if ( placement == terrain.size() ) {
						boolean res = joueurs.get(indiceJoueur).getCartes().get(carteSelectionnee).trier(terrain.get(terrain.size() - 1), 1);
						if ( res ) {
							terrain.add(joueurs.get(indiceJoueur).getCartes().get(getCarteSelectionnee()));
						} else {
							nouvelleCarteJoueur(indiceJoueur, getCarteSelectionnee());
						}	
					} else {
						boolean res1 = joueurs.get(indiceJoueur).getCartes().get(carteSelectionnee).trier(terrain.get(placement - 1), 1);
						boolean res2 = joueurs.get(indiceJoueur).getCartes().get(carteSelectionnee).trier(terrain.get(placement), 0);
						if ( res1 && res2 ) {
							terrain.add(placement, joueurs.get(indiceJoueur).getCartes().get(getCarteSelectionnee()));
						} else {
							nouvelleCarteJoueur(indiceJoueur, getCarteSelectionnee());
						}
					} 
					joueurs.get(indiceJoueur).supprimerCarte(getCarteSelectionnee());
					if (joueurs.get(indiceJoueur).getCartes().isEmpty()) {
						north.removeAll();
						north.validate();
						south.removeAll();
						south.validate();
						center.removeAll();
						center.validate();
						pane.removeAll();
						pane.validate();
						
						jouer();
					} else {
						raffraichirPanel(center, south, north, pane);
					}
				}
			});
			
			center.add(jb);
			center.add(jl);
			
			this.add(pane, BorderLayout.NORTH);
			this.pack();
			this.setVisible(true);
		}
		
		
		if( this.paquet.get(0) instanceof CarteCardline ){
			
			String[] indicateurs = { "Superficie", "Population", "Pib", "Pollution" };
			JComboBox listeIndicateurs = new JComboBox(indicateurs);

			JButton ok = new JButton("valider");
			ok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					switch ((String) listeIndicateurs.getSelectedItem()) {
					case "Superficie":
						Collections.sort((ArrayList<CarteCardline>) (ArrayList<?>) terrain, new ComparatorSuperficie());
						lf.modifierAttributCartes("superficie");
						break;
					case "Population":
						Collections.sort((ArrayList<CarteCardline>) (ArrayList<?>) terrain, new ComparatorPopulation());
						lf.modifierAttributCartes("population");
						break;
					case "Pib":
						Collections.sort((ArrayList<CarteCardline>) (ArrayList<?>) terrain, new ComparatorPib());
						lf.modifierAttributCartes("pib");
						break;
					case "Pollution":
						Collections.sort((ArrayList<CarteCardline>) (ArrayList<?>) terrain, new ComparatorPollution());
						lf.modifierAttributCartes("pollution");
						break;
					}
					north.removeAll();
					north.validate();
					south.removeAll();
					south.validate();
					center.removeAll();
					center.validate();
					pane.removeAll();
					pane.validate();

					jouer();
				}
				
			});
			north.add(listeIndicateurs);
			north.add(ok);
			
			this.add(north);
			this.pack();
			this.setVisible(true);
		}
	}

	/**
	 * methode permettant de construire les differents joueurs
	 * @param int nbCartes nombre de cartes
	 * @param int iterateurJoueur iterateurs de joueurs
	 * @param JPanel jp interface graphique
	 * @param int nbJoueur nombre de joueurs
	 * @param String jeu jeu choisi
	 */
	public void construireJoueur(int nbCartes, int iterateurJoueur, JPanel jp,int nbJoueur, String jeu){
		
		jp.revalidate();
		jp.repaint();
        
	    this.setTitle("Informations des joueurs");
	    this.setSize(971, 375);
	    this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	    
	    ImageIcon info = new ImageIcon("./data/data/Image/InfoJoueur.PNG");
        JLabel infoj = new JLabel(info);
        infoj.setBounds(0, 0, this.getWidth(), this.getHeight()/2-50);
        jp.add(infoj);
        
        ImageIcon nom = new ImageIcon("./data/data/Image/nom.png");
        JLabel nomj = new JLabel(nom);
        nomj.setBounds(0, 0, this.getWidth()/3+300, this.getHeight()/2+130);
        jp.add(nomj);
        
        ImageIcon age = new ImageIcon("./data/data/Image/age.png");
        JLabel agej = new JLabel(age);
        agej.setBounds(0, 0, this.getWidth()/3+300, this.getHeight()/2+350);
        jp.add(agej);
	    
	    JTextField n = new JTextField(" Joueur "+iterateurJoueur);
	    n.addFocusListener(new FocusAdapter() {
	        @Override
	        public void focusGained(FocusEvent e) {
	            n.setText("");
	        }
	    });
	    JTextField a = new JTextField(" Age");
	    a.addFocusListener(new FocusAdapter() {
	        @Override
	        public void focusGained(FocusEvent e) {
	            a.setText("");
	        }
	    });
	    Font police = new Font("Arial", Font.BOLD, 14);
	    n.setFont(police);
	    a.setFont(police);
	    n.setPreferredSize(new Dimension(150, 30));
	    a.setPreferredSize(new Dimension(150, 30));
	    n.setBounds(this.getWidth()/3+100, this.getHeight()/2-55, 150, 50);
	    a.setBounds(this.getWidth()/3+100,this.getHeight()/2+50, 150, 50);
	    jp.add(n);
	    jp.add(a);
	    
	    JButton valider=new JButton();
	    valider.setBounds(this.getWidth()-170,this.getHeight()-130,150,80);
	    valider.setIcon(new ImageIcon("./data/data/Image/valider.png"));
	    valider.addActionListener(new ActionListener(){

	    	int itj=iterateurJoueur;
	    	String j=jeu;
	    	
			@Override
			public void actionPerformed(ActionEvent arg0) {

				int iterateurCarte = 0;
				ArrayList<Carte> cartes = new ArrayList<Carte>();
				while( iterateurCarte < nbCartes ){
					Random r = new Random();
			        int nbAleatoire = r.nextInt(paquet.size());
			        cartes.add(paquet.get(nbAleatoire));
			        paquet.remove(paquet.get(nbAleatoire));
					iterateurCarte++;
				}
				joueurs.add(new Joueur(n.getText(), Integer.parseInt(a.getText()), cartes));
				
				jp.removeAll();
				jp.validate();
			
				itj+=1;
				demarrer(nbJoueur,jp,itj,jeu);
			}
	 
		  });
	    jp.add(valider);
	    	
	    ImageIcon icone = new ImageIcon("./data/data/Image/SteamPunk.jpg");
        JLabel fond = new JLabel(icone);
        fond.setBounds(0, 0, 971, 675);
        jp.add(fond);
	}

	/**
	 * methode permettant d'obtenir le joueur qui doit joeur en premier
	 * @return int indice du joueur qui joue en premier
	 */
	public int obtenirPremierJoueur(){
		int min = this.joueurs.get(0).getAge();
		int indicePremier = 0;
		for(int i = 1; i < this.joueurs.size(); i++){
			if(this.joueurs.get(i).getAge() < min){
				min = this.joueurs.get(i).getAge();
				indicePremier = i;
			}
		}
		
		return indicePremier;
	}
	
	/**
	 * methode permettant de rafraichir les jpanels
	 * @param JPanel north jpanel au nord
	 * @param JPanel center jpanel au centre
	 * @param JPanel south jpanel au sud
	 * @param JScrollPane pane panel permettant de scroll
	 */
	public void raffraichirPanel(JPanel north, JPanel center, JPanel south, JScrollPane pane){
		indiceJoueur++;
		if (indiceJoueur == joueurs.size()) {
			indiceJoueur = 0;
		}
		north.removeAll();
		north.validate();
		center.removeAll();
		center.validate();
		south.removeAll();
		south.validate();
		pane.removeAll();
		pane.validate();
		
		jouer();
	}
	
	/**
	 * methode permettant de remplacer une carte du joueur
	 * @param int indiceJoueur indice du joueur
	 * @param int carteJoueur indice de la carte du joueur
	 */
	public void nouvelleCarteJoueur(int indiceJoueur, int carteJoueur){
		this.boite.add(this.joueurs.get(indiceJoueur).getCartes().get(carteJoueur));
		Random r = new Random();
		int nbAleatoire = r.nextInt(this.paquet.size());
		this.joueurs.get(indiceJoueur).ajouterCarte(this.paquet.get(nbAleatoire));
		this.paquet.remove(this.paquet.get(nbAleatoire));
	}
	
	/**
	 * methode permettant de choisir le jeu
	 */
	public void acceuil(){
		
		JPanel jp= new JPanel();
		jp.setLayout(null);
		String jeu="";
        
	    this.setTitle("Choix du jeu");
	    this.setSize(1600, 800);
	    this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
        
        JButton tm=new JButton();
	    tm.setBounds((this.getWidth()/2)-200,50,150,80);
	    tm.setIcon(new ImageIcon("./data/data/Image/Jouer.PNG"));
	    tm.addActionListener(new ActionListener(){

	    	String j =jeu;
	    	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				j="timeline";
				jp.removeAll();
				jp.validate();
				selNbJoueur(jp,j);
			}
	 
		  });
	    jp.add(tm);
	    
	    JButton cl=new JButton();
	    cl.setBounds(this.getWidth()-200,50,150,80);
	    cl.setIcon(new ImageIcon("./data/data/Image/Jouer.PNG"));
	    cl.addActionListener(new ActionListener(){

	    	String j =jeu;
	    	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				j="cardline";
				jp.removeAll();
				jp.validate();
				selNbJoueur(jp,j);
			}
		  });
	    jp.add(cl);
	    
	    ImageIcon icone = new ImageIcon("./data/data/Image/Jeu.PNG");
        JLabel tl = new JLabel(icone);
        tl.setBounds(0, 0, 1600, 800);
        jp.add(tl);
        
	    this.setContentPane(jp);
	    
	    this.setResizable(false);
	    this.setVisible(true);
	}
	
	/**
	 * methode permettant de choisir le nombre de joueurs
	 * @param JPanel jp JPanel du jeu
	 * @param String jeu jeu choisi
	 */
	public void selNbJoueur(JPanel jp, String jeu){
		
		jp.revalidate();
		jp.repaint();
        
	    this.setTitle("Choix du nombre de joueur");
	    this.setSize(971, 375);
	    this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	    
	    ImageIcon selj = new ImageIcon("./data/data/Image/SelJ.PNG");
        JLabel selectj = new JLabel(selj);
        selectj.setBounds(0, 0, this.getWidth(), this.getHeight()/2-50);
        jp.add(selectj);
        
        int horizontal=this.getWidth()/5;
        
        for(int i=2;i<=5;i++){
        	
        	final int nbJoueurPartie=i;
        	
        	JButton bout=new JButton();
        	bout.setBounds(horizontal,this.getHeight()/2-50,80,80);
    	    bout.setIcon(new ImageIcon("./data/data/Image/"+i+".PNG"));
    	    bout.addActionListener(new ActionListener(){

    			@Override
    			public void actionPerformed(ActionEvent arg0) {
    				jp.removeAll();
    				jp.validate();
    				demarrer(nbJoueurPartie,jp,1,jeu);
    			}
    	 
    		  });
    	    horizontal+=170;
    	    jp.add(bout);
        }
        
        horizontal=this.getWidth()/3-45;
        for(int i=6;i<=8;i++){
        	
        	final int nbJoueurPartie=i;
        	
        	JButton bout=new JButton();
        	bout.setBounds(horizontal,this.getHeight()/2+50,80,80);
    	    bout.setIcon(new ImageIcon("./data/data/Image/"+i+".PNG"));
    	    bout.addActionListener(new ActionListener(){

    			@Override
    			public void actionPerformed(ActionEvent arg0) {
    				jp.removeAll();
    				jp.validate();
    				demarrer(nbJoueurPartie,jp,1,jeu);
    			}
    	 
    		  });
    	    horizontal+=172;
    	    jp.add(bout);
        }
        
        ImageIcon icone = new ImageIcon("./data/data/Image/SteamPunk.jpg");
        JLabel cnbj = new JLabel(icone);
        cnbj.setBounds(0, 0, 971, 675);
        jp.add(cnbj);
	}
	
	public int getCarteSelectionnee() {
		return carteSelectionnee;
	}
	
	public ArrayList<Carte> getTerrain() {
		return terrain;
	}
	
	final ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public ArrayList<Carte> getPaquet() {
		return paquet;
	}
	
	public int getIndiceJoueur() {
		return indiceJoueur;
	}

	public void setIndiceJoueur(int indiceJoueur) {
		this.indiceJoueur = indiceJoueur;
	}

	public void setCarteSelectionnee(int carteSelectionnee) {
		this.carteSelectionnee = carteSelectionnee;
	}

}
