package timelinejavaavancedenisnoel;

import java.util.Comparator;

public class ComparatorClassement implements Comparator<Joueur> {

	/**
	 * methode permettant de comparer les points dans le classement
	 */
	@Override
	public int compare(Joueur j1, Joueur j2) {
		return Integer.compare(j1.getPointsClassement(), j2.getPointsClassement());
	}
	
	

}
