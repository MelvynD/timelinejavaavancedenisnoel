package timelinejavaavancedenisnoel;

import java.util.Comparator;

public class ComparatorPollution implements Comparator<CarteCardline>{

	/**
	 * methode permettant de comparer la pollution
	 */
	@Override
	public int compare(CarteCardline o1, CarteCardline o2) {
		return Double.compare(o1.getPollution(), o2.getPollution());
	}

}
