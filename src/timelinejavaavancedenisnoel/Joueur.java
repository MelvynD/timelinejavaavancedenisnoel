package timelinejavaavancedenisnoel;

import java.io.Serializable;
import java.util.ArrayList;

public class Joueur implements Serializable{
	
	/**
	 * attribut du pseudo du joueur
	 */
	private String pseudonyme;
	/**
	 * attribut de l'age du joueur
	 */
	private int age;
	/**
	 * attribut de la liste de cartes du joueur
	 */
	private ArrayList<Carte> cartes;
	/**
	 * attribut permettant de savoir si le joueur a fini
	 */
	private boolean termine;
	/**
	 * attribut correspondant au nombre de points dans le classement du joueur
	 */
	private int pointsClassement;
	
	/**
	 * Constructeur du joueur
	 * @param String pseudonyme pseudo du joueur
	 * @param int age age du joueur
	 * @param ArrayList<Carte> cartes liste de cartes du joueur
	 */
	public Joueur(String pseudonyme, int age, ArrayList<Carte> cartes) {
		this.pseudonyme = pseudonyme;
		this.age = age;
		this.cartes = cartes;
		this.termine = false;
	}
	
	/**
	 * methode permettant d'ajouter une carte pour le joueur
	 * @param Carte c carte a ajouter
	 */
	public void ajouterCarte(Carte c){
		this.cartes.add(c);
	}
	
	@Override
	public String toString() {
		String res = "\nJoueur " + pseudonyme + "\n\n";
		for(int i = 0; i < this.cartes.size(); i++){
			res = res + this.cartes.get(i).getNom() + "\n";
		}
		return res;
	}

	/**
	 * methode permettant de supprimer une carte du joueur
	 * @param int indice indice de la carte a supprimer
	 */
	public void supprimerCarte(int indice){
		this.cartes.remove(this.cartes.get(indice));
	}

	public String getPseudonyme() {
		return pseudonyme;
	}

	public int getAge() {
		return age;
	}

	public ArrayList<Carte> getCartes() {
		return cartes;
	}
	
	public boolean isTermine() {
		return termine;
	}

	public void setTermine(boolean termine) {
		this.termine = termine;
	}
	
	public int getPointsClassement() {
		return pointsClassement;
	}

	public void setPointsClassement(int pointsClassement) {
		this.pointsClassement = pointsClassement;
	}

}
