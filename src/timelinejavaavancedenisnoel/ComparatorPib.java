package timelinejavaavancedenisnoel;

import java.util.Comparator;

public class ComparatorPib implements Comparator<CarteCardline>{

	/**
	 * methode permettant de comparer le pib 
	 */
	@Override
	public int compare(CarteCardline o1, CarteCardline o2) {
		return Integer.compare(o1.getPib(), o2.getPib());
	}

}
