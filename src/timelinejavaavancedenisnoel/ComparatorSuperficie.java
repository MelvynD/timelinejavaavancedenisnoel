package timelinejavaavancedenisnoel;

import java.util.Comparator;

public class ComparatorSuperficie implements Comparator<CarteCardline>{

	/**
	 * methode permettant de comparer la superficie
	 */
	@Override
	public int compare(CarteCardline o1, CarteCardline o2) {
		return Integer.compare(o1.getSuperficie(), o2.getSuperficie());
	}

}
