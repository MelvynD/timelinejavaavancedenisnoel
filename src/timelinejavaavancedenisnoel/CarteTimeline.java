package timelinejavaavancedenisnoel;

public class CarteTimeline extends Carte {
	 
	/**
	 * attribut correspondant a la date
	 */
	private int date;

	/**
	 * Constructeur de la classe CarteTimeline
	 * @param String nom nom de la carte 
	 * @param date date de la carte
	 * @param String carteNom nom de la face avant de la carte dans le fichier
	 * @param String carteNomReponse nom de la face arriere de la carte dans le fichier
	 */
	public CarteTimeline(String nom, int date, String carteNom, String carteNomReponse) {
		super(nom, carteNom, carteNomReponse);
		this.date = date;
	}

	/**
	 * methode permettant de comparer les cartes timeline
	 * @param Carte c2 autre carte � comparer 
	 * @param int placement placement de la carte
	 * @return
	 */
	@Override
	public boolean trier(Carte c2, int placement){
		int date = ((CarteTimeline) c2).date;
		boolean res = false;
		if (placement == 0) {
			if (date > this.date) {
				res = true;
			}
		} else if (placement == 1) {
			if (date < this.date) {
				res = true;
			}
		}
		return res;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + date;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarteTimeline other = (CarteTimeline) obj;
		if (date != other.date)
			return false;
		return true;
	}
	
	
	

}
