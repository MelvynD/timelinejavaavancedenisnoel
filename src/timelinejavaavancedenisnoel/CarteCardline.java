package timelinejavaavancedenisnoel;

import java.util.Comparator;

public class CarteCardline extends Carte {
	
	/**
	 * attribut de la superficie
	 */
	private int superficie;
	/**
	 * attribut de la population
	 */
	private int population;
	/**
	 * attribut du pib
	 */
	private int pib;
	/**
	 * attribut de la pollution
	 */
	private double pollution;
	/**
	 * attribut de comparaison
	 */
	private double attributComparaison;
	
	/**
	 * Constructeur de la classe CarteCardline
	 * @param String nom nom de la carte 
	 * @param String carteNom nom de la face avant de la carte dans le fichier
	 * @param String carteNomReponse nom de la face arriere de la carte dans le fichier
	 * @param int superficie superficie de la carte correspondante
	 * @param int population population de la carte correspondante
	 * @param int pib pib de la carte coeespondante
	 * @param double pollution pollution de la carte correspondante
	 * @param double attributComparaison comparaison
	 */
	public CarteCardline(String nom, String carteNom, String carteNomReponse, int superficie, int population, int pib, 
			double pollution, double attributComparaison) {
		super(nom, carteNom, carteNomReponse);
		this.superficie = superficie;
		this.population = population;
		this.pib = pib;
		this.pollution = pollution;
		this.attributComparaison = attributComparaison;
	}

	/**
	 * methode permettant de comparer les cartes cardline
	 * @param Carte c2 autre carte � comparer 
	 * @param int placement placement de la carte
	 * @return
	 */
	@Override
	public boolean trier(Carte c2, int placement) {
		double attribut = ((CarteCardline) c2).attributComparaison;
		boolean res = false;
		if (placement == 0) {
			if (attribut > this.attributComparaison) {
				res = true;
			}
		} else if (placement == 1) {
			if (attribut < this.attributComparaison) {
				res = true;
			}
		}
		return res;
	}
	
	public double getAttributComparaison() {
		return attributComparaison;
	}

	public void setAttributComparaison(double attributComparaison) {
		this.attributComparaison = attributComparaison;
	}

	public int getSuperficie() {
		return superficie;
	}

	public int getPopulation() {
		return population;
	}

	public int getPib() {
		return pib;
	}

	public double getPollution() {
		return pollution;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(attributComparaison);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + pib;
		temp = Double.doubleToLongBits(pollution);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + population;
		result = prime * result + superficie;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarteCardline other = (CarteCardline) obj;
		if (Double.doubleToLongBits(attributComparaison) != Double.doubleToLongBits(other.attributComparaison))
			return false;
		if (pib != other.pib)
			return false;
		if (Double.doubleToLongBits(pollution) != Double.doubleToLongBits(other.pollution))
			return false;
		if (population != other.population)
			return false;
		if (superficie != other.superficie)
			return false;
		return true;
	}

}
