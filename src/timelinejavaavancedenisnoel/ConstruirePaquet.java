package timelinejavaavancedenisnoel;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class ConstruirePaquet {
	
	/**
	 * constructeur correspondant au jeu
	 */
	private Jeu j;
	
	/**
	 * Constructeur de la classe ConstruirePaquet
	 * @param Jeu j partie jouee
	 */
	public ConstruirePaquet(Jeu j) {
		this.j = j;
	}

	/**
	 * methode permettant de construire le jeu timeline
	 */
	public void construireTimeline(){
		try {
			BufferedReader fi = new BufferedReader(new FileReader("./data/data/timeline/timeline.csv"));
			String ligne;
			ligne = fi.readLine();
			while ((ligne = fi.readLine()) != null) {
				String[] tab = ligne.split(";");
				Carte c = new CarteTimeline(tab[0], Integer.parseInt(tab[1]), "./data/data/timeline/cards/"+tab[2],
						"./data/data/timeline/cards/"+tab[2]+"_date");
				this.j.getPaquet().add(c);
			}
			fi.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * methode permettant de construire le jeu Cardline
	 */
	public void construireCardline(){
		try {
			BufferedReader fi = new BufferedReader(new FileReader("./data/data/cardline/cardline.csv"));
			String ligne;
			ligne = fi.readLine();
			while ((ligne = fi.readLine()) != null) {
				ligne = ligne.replace(",", ".");
				String[] tab = ligne.split(";");
				Carte c = new CarteCardline(tab[0], "./data/data/cardline/cards/"+tab[5], "./data/data/cardline/cards/"+tab[5]+"_reponse",
						Integer.parseInt(tab[1]), Integer.parseInt(tab[2]), 
						Integer.parseInt(tab[3]), Double.parseDouble(tab[4]), Double.parseDouble(tab[1]));
				this.j.getPaquet().add(c);
			}
			fi.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * methode permettant de modifier l'attribut a comparer pour les cartes cardline
	 * @param String nouveauComparateur nouveau comparateur ( pib, pollution... )
	 */
	public void modifierAttributCartes(String nouveauComparateur){
		try {
			BufferedReader fi = new BufferedReader(new FileReader("./data/data/cardline/cardline.csv"));
			String ligne;
			ligne = fi.readLine();
			while ((ligne = fi.readLine()) != null) {
				ligne = ligne.replace(",", ".");
				String[] tab = ligne.split(";");
				this.modifierAttributPaquet(nouveauComparateur, tab);
				this.modifierAttributTerrain(nouveauComparateur, tab);
				this.modifierAttributJoueurs(nouveauComparateur, tab);
			}
			fi.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * methode permettant de modifier l'attribut a comparer pour les cartes cardline dans le paquet
	 * @param String nouveauComparateur nouveau comparateur ( pib, pollution... )
	 * @param String[] tab tableau des donnees de la ligne
	 */
	public void modifierAttributPaquet(String nouveauComparateur, String[] tab) {
		for (int i = 0; i < this.j.getPaquet().size(); i++) {
			CarteCardline cartePaquet = (CarteCardline) j.getPaquet().get(i);
			if (cartePaquet.getNom().equals(tab[0])) {
				switch (nouveauComparateur) {
				case "population":
					double population = Double.parseDouble(tab[2]);
					cartePaquet.setAttributComparaison(population);
					break;
				case "pib":
					double pib = Double.parseDouble(tab[3]);
					cartePaquet.setAttributComparaison(pib);
					break;
				case "pollution":
					double pollution = Double.parseDouble(tab[4]);
					cartePaquet.setAttributComparaison(pollution);
					break;
				case "superficie":
					double superficie = Double.parseDouble(tab[1]);
					cartePaquet.setAttributComparaison(superficie);
					break;
				}
			}
		}
	}
	
	/**
	 * methode permettant de modifier l'attribut a comparer pour les cartes cardline sur le terrain
	 * @param String nouveauComparateur nouveau comparateur ( pib, pollution... )
	 * @param String[] tab tableau des donnees de la ligne
	 */
	public void modifierAttributTerrain(String nouveauComparateur, String[] tab) {
		for (int i = 0; i < this.j.getTerrain().size(); i++) {
			CarteCardline carteTerrain = (CarteCardline) j.getTerrain().get(i);
			if (carteTerrain.getNom().equals(tab[0])) {
				switch (nouveauComparateur) {
				case "population":
					double population = Double.parseDouble(tab[2]);
					carteTerrain.setAttributComparaison(population);
					break;
				case "pib":
					double pib = Double.parseDouble(tab[3]);
					carteTerrain.setAttributComparaison(pib);
					break;
				case "pollution":
					double pollution = Double.parseDouble(tab[4]);
					carteTerrain.setAttributComparaison(pollution);
					break;
				case "superficie":
					double superficie = Double.parseDouble(tab[1]);
					carteTerrain.setAttributComparaison(superficie);
					break;
				}
			}
		}
	}
	
	/**
	 * methode permettant de modifier l'attribut a comparer pour les cartes cardline des joueurs
	 * @param String nouveauComparateur nouveau comparateur ( pib, pollution... )
	 * @param String[] tab tableau des donnees de la ligne
	 */
	public void modifierAttributJoueurs(String nouveauComparateur, String[] tab) {
		for (int i = 0; i < this.j.getJoueurs().size(); i++) {
			for (int z = 0; z < this.j.getJoueurs().get(i).getCartes().size(); z++) {
				CarteCardline carteJoueur = (CarteCardline) j.getJoueurs().get(i).getCartes().get(z);
				if (carteJoueur.getNom().equals(tab[0])) {
					switch (nouveauComparateur) {
					case "population":
						double population = Double.parseDouble(tab[2]);
						carteJoueur.setAttributComparaison(population);
						break;
					case "pib":
						double pib = Double.parseDouble(tab[3]);
						carteJoueur.setAttributComparaison(pib);
						break;
					case "pollution":
						double pollution = Double.parseDouble(tab[4]);
						carteJoueur.setAttributComparaison(pollution);
						break;
					case "superficie":
						double superficie = Double.parseDouble(tab[1]);
						carteJoueur.setAttributComparaison(superficie);
						break;
					}
				}
			}
		}
	}

}
