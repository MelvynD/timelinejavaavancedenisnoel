package timelinejavaavancedenisnoel;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Classement {

	/**
	 * attribut correspondant a la liste des joueurs se situant dans le classement
	 */
	private ArrayList<Joueur> classement;
	
	/**
	 * Constructeur vide de la classe classement
	 */
	public Classement(){
		this.classement = new ArrayList<Joueur>();
	}
	
	/**
	 * methode permettant de reconstruire le classement
	 * @param Joueur j joueur qui a gagne la partie
	 */
	public void reconstruitClassement(Joueur j){
		try {
			ObjectInputStream fin = new ObjectInputStream(new FileInputStream("./Classement.txt"));
			this.classement = (ArrayList<Joueur>)(fin.readObject());
			boolean trouve = false;
			for(int i = 0; i < this.classement.size(); i++){
				if(this.classement.get(i).getPseudonyme().equals(j.getPseudonyme())){
					this.classement.get(i).setPointsClassement(this.classement.get(i).getPointsClassement()+1);
					trouve = true;
				}
			}
			if(!trouve){
				j.setPointsClassement(1);
				this.classement.add(j);
			}
			Collections.sort(this.classement, new ComparatorClassement());
			
			ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream("./Classement.txt"));
			fout.writeObject(this.classement);
			fout.close();
			fin.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Joueur> getClassement() {
		return classement;
	}
	
}
