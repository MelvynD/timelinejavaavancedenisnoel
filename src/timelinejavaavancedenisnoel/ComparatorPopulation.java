package timelinejavaavancedenisnoel;

import java.util.Comparator;

public class ComparatorPopulation implements Comparator<CarteCardline>{

	/**
	 * methode permettant de comparer la population
	 */
	@Override
	public int compare(CarteCardline o1, CarteCardline o2) {
		return Integer.compare(o1.getPopulation(), o2.getPopulation());
	}

}
