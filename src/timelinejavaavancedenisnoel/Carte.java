package timelinejavaavancedenisnoel;

import java.awt.Image;

public abstract class Carte {

	/**
	 * attribut correspondant au nom de la carte
	 */
	private String nom;
	/**
	 * attribut correspondant au nom de la face avant de la carte dans le fichier
	 */
	private String carteNom;
	/**
	 * attribut correspondant au nom de la face arriere de la carte dans le fichier
	 */
	private String carteNomReponse;
	
	/**
	 * constructeur de la carte
	 * @param String nom nom de la carte 
	 * @param String carteNom nom de la face avant de la carte dans le fichier
	 * @param String carteNomReponse nom de la face arriere de la carte dans le fichier
	 */
	public Carte(String nom, String carteNom, String carteNomReponse) {
		this.nom = nom;
		this.carteNom = carteNom;
		this.carteNomReponse = carteNomReponse;
	}
	
	/**
	 * methode permettant de comparer les cartes en fonction du type de carte
	 * @param Carte c2 autre carte � comparer 
	 * @param int placement placement de la carte
	 * @return
	 */
	public abstract boolean trier(Carte c2, int placement);
	
	public String getCarteNomReponse() {
		return carteNomReponse;
	}

	public String getCarteNom() {
		return carteNom;
	}

	public String getNom() {
		return nom;
	}

}
